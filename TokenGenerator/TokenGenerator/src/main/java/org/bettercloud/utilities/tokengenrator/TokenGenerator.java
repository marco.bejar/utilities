package org.bettercloud.utilities.tokengenrator;

import java.security.GeneralSecurityException;
import java.security.InvalidParameterException;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TokenGenerator {

  private static final String AUTH_HEADER = "authorization";

  private static final String BC_DATE_HEADER = "x-bc-date";

  private static final String VERSION = "BC1-HMAC-SHA256";

  private static final String ALGORITHM = "HmacSHA256";

  private static final String AUTHORIZATION_FORMAT_STRING =
      "%s keyid=%s& signature=%s signedheaders=date,uri";

  private static final String LOOKUP_VALIDATION_ERROR_MESSAGE = "lookup must not be null or empty";

  private static final String SECRET_VALIDATION_ERROR_MESSAGE = "secret must not be null or empty";

  private static final String ERROR_MESSAGE = "Error: {}";

  private static final String NULL_OR_EMPTY_ERROR_MESSAGE = "Path must not be null or empty";

  private final String lookup;

  private final Mac mac;

  public TokenGenerator(String lookup, String secret) {

    Utils.validateStringNotEmpty(lookup, LOOKUP_VALIDATION_ERROR_MESSAGE);
    Utils.validateStringNotEmpty(secret, SECRET_VALIDATION_ERROR_MESSAGE);

    try {
      mac = Mac.getInstance(ALGORITHM);
      mac.init(new SecretKeySpec(secret.getBytes(), ALGORITHM));
    } catch (GeneralSecurityException generalSecurityException) {
      log.error(ERROR_MESSAGE, generalSecurityException.getLocalizedMessage());
      throw new InvalidParameterException();
    }

    this.lookup = lookup;
  }

  public Map<String, String> generateHMACHeaders(String path) {

    Utils.validateStringNotEmpty(path, NULL_OR_EMPTY_ERROR_MESSAGE);

    Map<String, String> headers = new HashMap<>();

    String now = ZonedDateTime.now(ZoneOffset.UTC).format(DateTimeFormatter.ISO_DATE_TIME);
    String signature = buildSignature(path, now);
    String authorization = String.format(AUTHORIZATION_FORMAT_STRING, VERSION, lookup, signature);

    headers.put(AUTH_HEADER, authorization);
    headers.put(BC_DATE_HEADER, now);

    return headers;
  }

  private String buildSignature(String path, String now) {
    String message = now + "," + path;
    byte[] hmacSha256 = mac.doFinal(message.getBytes());
    return Base64.getEncoder().encodeToString(hmacSha256);
  }
}
