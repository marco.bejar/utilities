package org.bettercloud.utilities.tokengenrator;

import com.google.cloud.secretmanager.v1.AccessSecretVersionResponse;
import com.google.cloud.secretmanager.v1.SecretManagerServiceClient;
import com.google.cloud.secretmanager.v1.SecretVersionName;
import java.io.IOException;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public final class SecretResolver {

  private static final String SECRET_MANAGER_PREFIX = "sm://";
  private static final String SECRET_MANAGER_PROJECT_ID = "bcx-sm-nonprod";
  public static final String ERROR_MESSAGE = "Error: {}";
  public static final String CREATE_GCP_SECRET_ERROR_MESSAGE =
      "Unable to create GCP secret manager client";
  public static final String RESOLVING_GCP_SECRET_MESSAGE = "Resolving GCP secret {}/{}";
  public static final String SECRET_VERSION = "latest";
  private static SecretResolver INSTANCE;
  private final SecretManagerServiceClient client;

  private SecretResolver() {
    try {
      client = SecretManagerServiceClient.create();
    } catch (IOException ioException) {
      log.error(ERROR_MESSAGE, ioException.getLocalizedMessage());
      throw new SecretResolutionException(CREATE_GCP_SECRET_ERROR_MESSAGE, ioException);
    }
  }

  public static SecretResolver getInstance() {
    if (Objects.isNull(INSTANCE)) {
      INSTANCE = new SecretResolver();
    }
    return INSTANCE;
  }

  /**
   * Checks if a property key starts with the Google Cloud Secret Manager configuration prefix:
   * sm://
   *
   * @param propertyValue the property to check
   * @return true if it starts with the GCP secret manager prefix
   */
  public static boolean isGcpSecretKey(String propertyValue) {
    return (!Utils.isEmpty(propertyValue)) && propertyHasGcpSecretStructure(propertyValue);
  }

  private static boolean propertyHasGcpSecretStructure(String propertyValue) {
    return propertyValue.startsWith(SECRET_MANAGER_PREFIX)
        && !propertyValue.substring(SECRET_MANAGER_PREFIX.length()).isBlank();
  }

  /**
   * Resolves a GCP Secret Manager secret key (prefixed with sm://). If value is not prefixed with
   * sm:// this return the argument back unmodified.
   *
   * @param propertyValue the GCP Secret Manager secret key (prefixed with sm://)
   * @return the resolved secret
   */
  public String resolveSecret(String propertyValue) {
    if (isGcpSecretKey(propertyValue)) {
      return getSecretKey(propertyValue);
    } else {
      return propertyValue;
    }
  }

  private String getSecretKey(String propertyValue) {

    String secretKey = propertyValue.substring(SECRET_MANAGER_PREFIX.length());
    log.debug(RESOLVING_GCP_SECRET_MESSAGE, SECRET_MANAGER_PROJECT_ID, propertyValue);

    SecretVersionName secretVersionName = createSecretVersionName(secretKey);
    AccessSecretVersionResponse response = client.accessSecretVersion(secretVersionName);
    Utils.verifyChecksum(response);

    return response.getPayload().getData().toStringUtf8();
  }

  private static SecretVersionName createSecretVersionName(String secretKey) {
    return SecretVersionName.newBuilder()
        .setProject(SECRET_MANAGER_PROJECT_ID)
        .setSecret(secretKey)
        .setSecretVersion(SECRET_VERSION)
        .build();
  }
}
