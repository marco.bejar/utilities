package org.bettercloud.utilities.tokengenrator;

import com.google.cloud.secretmanager.v1.AccessSecretVersionResponse;
import java.util.Objects;
import java.util.zip.CRC32C;
import java.util.zip.Checksum;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public final class Utils {

  public static void validateStringNotEmpty(String string, String errorMessage) {
    if (isEmpty(string)) {
      throw new IllegalArgumentException(errorMessage);
    }
  }

  public static boolean isEmpty(String propertyValue) {
    return Objects.isNull(propertyValue) || propertyValue.isBlank();
  }

  public static void verifyChecksum(AccessSecretVersionResponse response) {
    byte[] data = response.getPayload().getData().toByteArray();
    Checksum checksum = new CRC32C();
    checksum.update(data, 0, data.length);
    if (responseChecksumError(response, checksum)) {
      log.error("Data corruption detected while fetching secret");
      throw new SecretResolutionException("Data corruption detected while fetching secret");
    }
  }

  private static boolean responseChecksumError(
      AccessSecretVersionResponse response, Checksum checksum) {
    return response.getPayload().getDataCrc32C() != checksum.getValue();
  }
}
