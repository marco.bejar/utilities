package org.bettercloud.utilities.tokengenrator;

import lombok.Getter;

@Getter
public class HmacProperties {

  private final String key;
  private final String value;
  private final SecretResolver secretResolver = SecretResolver.getInstance();

  public HmacProperties(String key, String value){
    this.key = resolveSecret(key);
    this.value = resolveSecret(value);
  }

  private String resolveSecret(String string) {
    if (SecretResolver.isGcpSecretKey(string)) {
      return secretResolver.resolveSecret(string);
    } else {
      return string;
    }
  }
}
