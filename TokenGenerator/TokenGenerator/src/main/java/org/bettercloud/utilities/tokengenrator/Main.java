package org.bettercloud.utilities.tokengenrator;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;

@Slf4j
public class Main {

  public static void main(String[] args) {

    final HmacProperties hmac =
        new HmacProperties(
            "BetterCloud_hmac_2019-08-22-1", "hIgivImi5Hb7ouzBd18iGL17jTd3xGv6FpcGULgHQ");

    final String baseUri =
        "https://x-holding.devbettercloud.com/directory/users/v1/hmac/bcusers/external/";
    final String uri = baseUri + "00uhycai0dDMPXmys4x6";

    final HttpUriRequest request =
        RequestBuilder.get()
            .setUri(uri)
            .addParameter("tenantId", "308d4edc-4178-11eb-a7ef-97c2c1a374a4")
            .addParameter("integrationId", "3065f024-4178-11eb-9b21-2b2935b8ce8d")
            .build();

    log.info(request.getURI().getPath());

    final TokenGenerator tokenGenerator = new TokenGenerator(hmac.getKey(), hmac.getValue());

    tokenGenerator
        .generateHMACHeaders(request.getURI().getPath())
        .forEach((k, v) -> System.out.println(k + "=" + v));
  }
}
